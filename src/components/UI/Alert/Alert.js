import React from 'react';
import './Alert.css';

const Alert = (props) => {
   return (
     <div className="alert-content" style={{
        left: props.coordinates[0],
        top: props.coordinates[1],
     }}>
        <div className={"alert-text " + props.type} onClick={!props.dismissed ? props.dismiss : null}>
           {props.children}
           {props.dismissed ?
             <button className="alert-closeBtn" onClick={props.dismiss}>X</button> : null}
        </div>

     </div>
   );
};

export default Alert;